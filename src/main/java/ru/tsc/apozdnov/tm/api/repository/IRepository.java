package ru.tsc.apozdnov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.enumerated.Sort;
import ru.tsc.apozdnov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @Nullable
    M findOneById(String id);

    @Nullable
    M findOneByIndex(Integer index);

    @Nullable
    M removeById(String id);

    @Nullable
    M removeByIndex(Integer index);

    @Nullable
    M remove(M model);

    @Nullable
    M add(M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll(Comparator comparator);

    @NotNull
    List<M> findAll(Sort sort);

    @NotNull
    List<M> findAll();

    boolean existsById(String id);

    int getSize();

    void clear();

    void removeAll(Collection<M> collection);

}
