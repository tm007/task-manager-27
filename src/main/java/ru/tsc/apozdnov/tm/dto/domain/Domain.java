package ru.tsc.apozdnov.tm.dto.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.model.Task;
import ru.tsc.apozdnov.tm.model.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class Domain implements Serializable {

    private static final long serialVersionUID = 1;

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private Date created = new Date();

    @NotNull
    private List<User> userList = new ArrayList<>();

    @NotNull
    private List<Project> projectList = new ArrayList<>();

    @NotNull
    private List<Task> taskList = new ArrayList<>();

}
