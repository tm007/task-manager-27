package ru.tsc.apozdnov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.enumerated.Status;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-complete-by-index";

    @NotNull
    public static final String DESCRIPTION = "Complete project by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("***** COMPLETE PROJECT BY INDEX ****");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();
        serviceLocator.getProjectService().changeStatusByIndex(userId, index, Status.COMPLETED);
    }

}
