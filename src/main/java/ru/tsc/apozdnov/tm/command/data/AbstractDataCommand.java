package ru.tsc.apozdnov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.command.AbstractCommand;
import ru.tsc.apozdnov.tm.dto.domain.Domain;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
        domain.setUserList(serviceLocator.getUserService().findAll());
        domain.setProjectList(serviceLocator.getProjectService().findAll());
        domain.setTaskList(serviceLocator.getTaskService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getUserService().set(domain.getUserList());
        serviceLocator.getProjectService().set(domain.getProjectList());
        serviceLocator.getTaskService().set(domain.getTaskList());
        serviceLocator.getAuthService().logout();
    }

}