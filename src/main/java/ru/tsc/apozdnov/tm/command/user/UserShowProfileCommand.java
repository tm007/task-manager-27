package ru.tsc.apozdnov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.enumerated.RoleType;
import ru.tsc.apozdnov.tm.model.User;

public class UserShowProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-show-profile";

    @NotNull
    public static final String DESCRIPTION = "Show user info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public RoleType[] getRoleType() {
        return RoleType.values();
    }

    @Override
    public void execute() {
        @NotNull final User user = serviceLocator.getAuthService().getUser();
        showUser(user);
    }

}