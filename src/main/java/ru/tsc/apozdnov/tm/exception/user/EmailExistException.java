package ru.tsc.apozdnov.tm.exception.user;

public class EmailExistException extends AbstractUserException {

    public EmailExistException() {
        super("Error!!! Email already exist!");
    }

}
