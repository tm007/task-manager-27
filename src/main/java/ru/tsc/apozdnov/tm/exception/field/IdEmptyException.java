package ru.tsc.apozdnov.tm.exception.field;

public final class IdEmptyException extends AbstractFieldException {

    public IdEmptyException() {
        super("FAULT! Id is empty!!!");
    }

}
