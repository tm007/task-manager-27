package ru.tsc.apozdnov.tm.exception.field;

public final class DescriptionEmptyException extends AbstractFieldException {

    public DescriptionEmptyException() {
        super("FAULT!! Description is empty!!!");
    }

}
